﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitIcon : MonoBehaviour
{
    public Text code;
    public Text[] facilitatorNames;
    public Image background;

    public void Init(UnitObject unit, int score)
    {
        code.text = unit.name;
        for(int i = 0; i < unit.facilitators.Count; i++)
        {
            facilitatorNames[i].text = unit.facilitators[i].ToString();
        }

        if(score > 700)
        {
            background.color = Color.green;
        }
        else if(score > 500)
        {
            background.color = Color.yellow;
        }
        else
        {
            background.color = Color.red;
        }
    }
}
