﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadUI : MonoBehaviour
{
    public Image loadbar;
    public Text statusText;

    private void Awake()
    {
        FindObjectOfType<Organiser>().status += UpdateUI;
    }

    void UpdateUI(Status s)
    {
        loadbar.fillAmount =  (float)(s.iteration+1) / (float)s.totalIterations;
        statusText.text = "Iteration " + s.iteration.ToString() + "/" + s.totalIterations + ", best points:" + s.bestpoints.ToString();
    }
}
