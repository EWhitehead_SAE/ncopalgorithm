﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Organiser : MonoBehaviour
{
public List<UnitObject> units;
    public int timeslots = 4;
    public int spaces = 3;

    UnitObject[,] bestSchedule;
    int bestpoints;

    UnitObject[,] workingSchedule;
    int points = 10000;

    public int iterations;

    public StatusNotification status;
    public UnityEvent showLoadUI;
    public UnityEvent hideLoadUI;
    public UnityEvent hideDisplayUI;
    public UnityEvent showDisplayUI;

    public void UpdateTimeslots(string input)
    {
        int.TryParse(input, out timeslots);
    }
    public void UpdateIterations(string input)
    {
        int.TryParse(input, out iterations);
    }
    public void UpdateSpaces(string input)
    {
        int.TryParse(input, out spaces);
    }

    public void InitialiseAndRun()
    {
        bestSchedule = new UnitObject[spaces, timeslots];
        points = 1000;
        bestpoints = 0;
        hideDisplayUI.Invoke();
        showLoadUI.Invoke();
        StartCoroutine(Run());
    }

    IEnumerator Run()
    {
        for (int i = 0; i < iterations; i++)
        {
            workingSchedule = new UnitObject[spaces, timeslots];
            Populate();
            //Debug.Log("Run : " + i.ToString());
            status.Invoke(new Status(iterations, i, bestpoints));
            yield return null;
        }

        Output(bestSchedule);
    }

    void Populate()
    {
        foreach(UnitObject u in units)
        {
            int x = Random.Range(0, workingSchedule.GetLength(0));
            int y = Random.Range(0, workingSchedule.GetLength(1));

            while (workingSchedule[x, y] != null)
            {
                x = Random.Range(0, workingSchedule.GetLength(0));
                y = Random.Range(0, workingSchedule.GetLength(1));
            }

            if(workingSchedule[x,y] == null)
            {
                workingSchedule[x, y] = u;
            }
        }
        Debug.Log("Populated");
        Evaluate();
    }

    void Evaluate()
    {
        //by timeslot
        for (int y = 0; y < workingSchedule.GetLength(1); y++)
        {
            List<Facilitators> f = new List<Facilitators>();
            //int rowPoints = 1000;
            //cross units
            for (int x = 0; x < workingSchedule.GetLength(0); x++)
            {
                if(workingSchedule[x,y] == null)
                {
                    continue;
                }
                f.AddRange(workingSchedule[x, y].facilitators);
            }

            //Check facilitators, punish less;
            Dictionary<Facilitators, int> facilitators = new Dictionary<Facilitators, int>();
            f.ForEach(s =>
            {
                if (facilitators.ContainsKey(s))
                {
                    facilitators[s]++;
                }
                else
                {
                    facilitators[s] = 1;
                }
            });
            
            foreach(KeyValuePair<Facilitators,int> v in facilitators)
            {
                if(v.Value > 1)
                {
                    points -= v.Value * 100;
                }
            }
        }

        if(points > bestpoints)
        {
            bestpoints = points;
            bestSchedule = workingSchedule;
        }
        //Output(workingSchedule);
        points = 1000;
    }

    void Output(UnitObject[,] schedule)
    {
        for (int y = 0; y < schedule.GetLength(1); y++)
        {
            Debug.Log(y);
            string n = "";
            for (int x = 0; x < schedule.GetLength(0); x++)
            {
                n += schedule[x, y] == null ? "NULL" : schedule[x, y].name + " : ";
            }
            Debug.Log(n);
        }
        Debug.Log(bestpoints);
        hideLoadUI.Invoke();
        showDisplayUI.Invoke();
    }
}

public struct Status
{
    public int totalIterations;
    public int iteration;
    public int bestpoints;

    public Status (int _ti, int _i, int _bp)
    {
        totalIterations = _ti;
        iteration = _i;
        bestpoints = _bp;
    }
}

public delegate void StatusNotification(Status s);