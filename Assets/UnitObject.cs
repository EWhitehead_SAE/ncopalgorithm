﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Unit")][System.Serializable]
public class UnitObject : ScriptableObject
{
    [SerializeField]
    public int priority;
    [SerializeField]
    public List<Facilitators> facilitators;
}

public enum Facilitators {EdWhitehead, ZhiaZariko, NathanJensen, FaeDaunt, ErikaVerkaaik, HaigenPennycuick, ThejaswiPadmanabha, SteveHalliwell, GlennSpoors, IainMcManus, KarlPytte
, GregQuincey, GeoffHill, RexSmeal, CamBonde, AdamHo, AlanMurray, SaulAlexander, NewStaff, CliffSharif, AaronGoss, AaronWilliams, TonyParamenter, PeterZhao, MattDyet, JacobKreck, TBD, NA}
